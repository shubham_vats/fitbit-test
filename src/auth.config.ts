import { isPlatform } from '@ionic/react';

export const domain = 'dev-3n8sbnok.us.auth0.com';
export const clientId = 'izP6jU8xmP20qPPbHKF3pYy1rnAyBn6w';
const appId = 'com.auth0.fitbitTest';

// Use `auth0Domain` in string interpolation below so that it doesn't
// get replaced by the quickstart auto-packager
const auth0Domain = domain;

export const callbackUri = isPlatform('desktop')
  ? 'http://localhost:3000'
  : `${appId}://${auth0Domain}/capacitor/${appId}/callback`;
