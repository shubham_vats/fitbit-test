import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.auth0.fitbitTest',
  appName: 'fitbit-auth0-test',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
